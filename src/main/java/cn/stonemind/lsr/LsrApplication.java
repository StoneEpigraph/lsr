package cn.stonemind.lsr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LsrApplication {

    public static void main(String[] args) {
        SpringApplication.run(LsrApplication.class, args);
    }

}
